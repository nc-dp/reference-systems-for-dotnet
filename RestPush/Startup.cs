using System;
using System.IO;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Digst.DigitalPost.SSLClient.Clients;
using Digst.DigitalPost.Systems.RestPush.Sender.Configuration;
using Digst.DigitalPost.Systems.RestPush.Sender.Services;
using Digst.DigitalPost.UtilityLibrary.Memos.Configuration;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Logging;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.MemoBuilder;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Parser;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Persistence;
using Digst.DigitalPost.UtilityLibrary.Receipts.Configuration;
using Digst.DigitalPost.UtilityLibrary.Receipts.Sender;
using Digst.DigitalPost.UtilityLibrary.Receipts.Services;
using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Digst.DigitalPost.Systems.RestPush
{
    public class Startup
    {
        private readonly string ClientCertificateLocation = "CLIENT_CERTIFICATE_LOCATION";

        private readonly string ClientCertificatePassword = "CLIENT_CERTIFICATE_PASSWORD";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // AddNewtonsoftJson method helps to specify any additional Json options or settings including as input and output formats
            services.AddControllers().AddNewtonsoftJson();

            services.AddAuthentication(
                    CertificateAuthenticationDefaults.AuthenticationScheme)
                .AddCertificate(options =>
                {
                    options.AllowedCertificateTypes = CertificateTypes.All;
                    options.Events = new CertificateAuthenticationEvents
                    {
                        OnCertificateValidated = context =>
                        {
                            Claim[] claims =
                            {
                                new Claim(ClaimTypes.NameIdentifier, context.ClientCertificate.Subject,
                                    ClaimValueTypes.String, context.Options.ClaimsIssuer),
                                new Claim(ClaimTypes.Name, context.ClientCertificate.Subject, ClaimValueTypes.String,
                                    context.Options.ClaimsIssuer)
                            };

                            context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                            context.Success();

                            return Task.CompletedTask;
                        },

                        OnAuthenticationFailed = context =>
                        {
                            ILogger<Startup> logger =
                                context.HttpContext.RequestServices.GetService<ILogger<Startup>>();
                            logger.LogError(context.Exception, "Failed authentication");
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddSingleton(BindSenderSystemConfiguration());
            services.AddSingleton(BindRecipientSystemConfiguration());
            services.AddSingleton(BindMemoConfiguration());

            services.AddScoped<IMemoService, ParseMemoService>();
            services.AddScoped<IBusinessReceiptFactory, BusinessReceiptFactory>();
            services.AddScoped<IBusinessReceiptService, BusinessReceiptService>();
            services.AddScoped<IMemoBuilder, MemoBuilder>();
            services.AddScoped<IMeMoPersister, MeMoPersister>();
            services.AddScoped<IMemoLogging, MemoLoggingService>();
            services.AddScoped<MeMoPushService>();
            services.AddScoped<IReceivedBusinessReceiptLoggingService, ReceivedBusinessReceiptLoggingService>();

            services.AddHttpClient<RestClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    HttpClientHandler handler = new HttpClientHandler();
                    handler.ClientCertificates.Add(LoadClientCertificate());
                    return handler;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Console.WriteLine("HostingEnvironmentName: '{0}'", env.EnvironmentName);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseStaticFiles();


            IServiceScope scope = app.ApplicationServices.CreateScope();

            MeMoPushService memoPushService = scope.ServiceProvider.GetRequiredService<MeMoPushService>();
            memoPushService.SendMeMo();
            memoPushService.SendMeMoTar();
        }

        private X509Certificate2 LoadClientCertificate()
        {
            string certificateFileName = Environment.GetEnvironmentVariable(ClientCertificateLocation);
            string certificatePassword = Environment.GetEnvironmentVariable(ClientCertificatePassword);

            return new X509Certificate2(Path.Combine(certificateFileName), certificatePassword);
        }

        private SenderSystemConfiguration BindSenderSystemConfiguration()
        {
            SenderSystemConfiguration config = new SenderSystemConfiguration();
            Configuration.Bind("SenderSystem", config);

            return config;
        }

        private RecipientSystemConfiguration BindRecipientSystemConfiguration()
        {
            RecipientSystemConfiguration config = new RecipientSystemConfiguration();
            Configuration.Bind("RecipientSystem", config);

            return config;
        }

        private MemoConfiguration BindMemoConfiguration()
        {
            MemoConfiguration config = new MemoConfiguration();
            Configuration.Bind("SenderSystem:Memo", config);

            return config;
        }
    }
}