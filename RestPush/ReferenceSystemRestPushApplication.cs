using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Hosting;

namespace Digst.DigitalPost.Systems.RestPush
{
    public class ReferenceSystemRestPushApplication
    {
        private static readonly string ServerCertificateLocation = "SERVER_CERTIFICATE_LOCATION";

        private static readonly string ServerCertificatePassword = "SERVER_CERTIFICATE_PASSWORD";

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureKestrel(options =>
                    {
                        string certificateFileName = Environment.GetEnvironmentVariable(ServerCertificateLocation);
                        string certificatePassword = Environment.GetEnvironmentVariable(ServerCertificatePassword);

                        X509Certificate2 cert =
                            new X509Certificate2(File.ReadAllBytes(Path.Combine(certificateFileName)),
                                certificatePassword);

                        options.ConfigureHttpsDefaults(o =>
                        {
                            o.ServerCertificate = cert;
                            o.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
                        });
                    });
                });
        }
    }
}