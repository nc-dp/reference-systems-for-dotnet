﻿using System;
using System.IO;
using Digst.DigitalPost.UtilityLibrary.Receipts.Models;
using Digst.DigitalPost.UtilityLibrary.Receipts.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Digst.DigitalPost.UtilityLibrary.Test
{
    [TestClass]
    public class BusinessReceiptFactoryTest
    {
        private static IBusinessReceiptFactory _businessReceiptFactory;

        private static readonly string validMemo = "Resources/Memos/eb03da92-fb62-4880-976e-3f92bd25e2a4";

        private static IFormFile _memoFile;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            _businessReceiptFactory = new BusinessReceiptFactory();
            FileStream stream = File.OpenRead(validMemo);
            _memoFile = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));
        }

        [TestMethod]
        public void Create_NegativeReceipt_Should_Give_Completed_Receipt_Status()
        {
            Receipt positiveReceipt = _businessReceiptFactory.CreatePositiveBusinessReceipt(_memoFile.FileName);
            Assert.AreEqual(ReceiptStatus.Completed, positiveReceipt.ReceiptStatus);
            Assert.AreEqual(new Guid("eb03da92-fb62-4880-976e-3f92bd25e2a4"), positiveReceipt.MessageUuid);
        }

        [TestMethod]
        public void Create_NegativeReceipt_Should_Give_Invalid_Receipt_Status()
        {
            Receipt positiveReceipt = _businessReceiptFactory.CreateNegativeBusinessReceipt(_memoFile.FileName);
            Assert.AreEqual(ReceiptStatus.Invalid, positiveReceipt.ReceiptStatus);
            Assert.AreEqual(new Guid("eb03da92-fb62-4880-976e-3f92bd25e2a4"), positiveReceipt.MessageUuid);
        }
    }
}