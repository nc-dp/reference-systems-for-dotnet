﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Digst.DigitalPost.SSLClient.Clients;
using Digst.DigitalPost.UtilityLibrary.Receipts.Configuration;
using Digst.DigitalPost.UtilityLibrary.Receipts.Models;
using Digst.DigitalPost.UtilityLibrary.Receipts.Sender;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace Digst.DigitalPost.UtilityLibrary.Test
{
    [TestClass]
    public class BusinessReceiptServiceTests
    {
        private static IBusinessReceiptService _businessReceiptService;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Mock<ILogger<BusinessReceiptService>> receiptLoggerMock = new Mock<ILogger<BusinessReceiptService>>();
            Mock<ILogger<RestClient>> restClientLoggerMock = new Mock<ILogger<RestClient>>();

            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK
                })
                .Verifiable();

            HttpClient httpClient = new HttpClient(handlerMock.Object);
            RestClient restClient = new RestClient(restClientLoggerMock.Object, httpClient);

            _businessReceiptService = new BusinessReceiptService(restClient, receiptLoggerMock.Object, new RecipientSystemConfiguration
            {
                ReceiptEndpoint = "https://api.test.digitalpost.dk/apis/v1/memos/{0}/receipt"
            });
        }

        [TestMethod]
        public void Send_BusinessReceipt_Should_Return_OK_Response()
        {
            Receipt receipt = new Receipt
            {
                MessageUuid = Guid.NewGuid(),
                TimeStamp = DateTime.Now,
                TransmissionId = Guid.NewGuid()
            };

            Task<HttpResponseMessage> result =
                _businessReceiptService.SendRestBusinessReceiptToNgDP(receipt, receipt.MessageUuid.ToString());
            Assert.AreEqual(result.Result.StatusCode, HttpStatusCode.OK);
        }
    }
}