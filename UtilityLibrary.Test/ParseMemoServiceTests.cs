﻿using System.IO;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Parser;
using Dk.Digst.Digital.Post.Memolib.Model;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using File = System.IO.File;

namespace Digst.DigitalPost.UtilityLibrary.Test
{
    [TestClass]
    public class ParseMemoServiceTests
    {
        private static IMemoService _parseMemoService;

        private static readonly string validMemo = "Resources/Memos/eb03da92-fb62-4880-976e-3f92bd25e2a4";

        private static readonly string invalidMemo = "Resources/Memos/MeMo_Full_Example_Invalid.xml";

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Mock<ILogger<ParseMemoService>> loggerMock = new Mock<ILogger<ParseMemoService>>();

            _parseMemoService = new ParseMemoService(loggerMock.Object);
        }

        [TestMethod]
        public void Parse_Valid_Memo_Should_Return_Valid_Header()
        {
            FileStream memoStream = File.OpenRead(validMemo);

            MessageHeader messageHeader = _parseMemoService.ParseMemoHeader(memoStream);

            Assert.AreEqual("eb03da92-fb62-4880-976e-3f92bd25e2a4", messageHeader.messageUUID);
        }

        [TestMethod]
        public void Parse_Invalid_Memo_Should_Return_Null_Header()
        {
            FileStream memoStream = File.OpenRead(invalidMemo);

            MessageHeader messageHeader = _parseMemoService.ParseMemoHeader(memoStream);

            Assert.AreEqual(null, messageHeader);
        }
    }
}