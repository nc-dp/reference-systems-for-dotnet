## **Reference-systems**

The main purpose of this application is to provide a reference implementation of sender- and recipient-system integration with Next Generation Digital Post (NgDP). It is intended to give insight into the process of sending and receiving MeMos and receipts to and from NgDP, by providing examples, which can assist respective implementers in their own integration to NgDP.

The reference implementation is thus meant as a general guideline more so than a specific recipe for integration.

The reference implementation provides examples of system integration with NgDP for the following protocols: 

    - REST_PUSH (sending and receiving) 
    - REST_PULL (receiving) 
    - SFTP (sending)

### **Dependencies**

This application is dependent on memo-lib-dot-net, available here: https://bitbucket.org/nc-dp/memo-lib-dot-net/src/master/

A range of libraries sourced from the Nuget is used to simplify the code, support functionality and reduce boilerplate. MavenCentral: https://www.nuget.org/

webinars
A webinar has been created to provide an overview of the recipient-systems and how they integrate to NgDP. Available here: https://digst.dk/it-loesninger/naeste-generation-digital-post/for-myndigheder-og-it-leverandoerer/for-it-leverandoerer/referenceimplementeringer-og-memo-lib/

### **Respository structure**

#### Overall

This application is separated into multiple sub-projects, each representing areas of responsibility. Sub-projects that include sending and receiving functionality are runnable as stand alone .NET Applications. When run, these applications will trigger parts of the integration flow, and mimic much of the actual process for sending and receiving MeMos and Business Receipts for the given protocol.

As an example, running the ReferenceSystemRestPushApplication in the RestPush sub-project:



    public class ReferenceSystemRestPushApplication
    {
        private readonly static string ServerCertificateLocation = "SERVER_CERTIFICATE_LOCATION";
        private readonly static string ServerCertificatePassword = "SERVER_CERTIFICATE_PASSWORD";

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureKestrel(options =>
                    {
                        string certificateFileName = Environment.GetEnvironmentVariable(ServerCertificateLocation);
                        string certificatePassword = Environment.GetEnvironmentVariable(ServerCertificatePassword);

                        X509Certificate2 cert = new X509Certificate2(File.ReadAllBytes(Path.Combine(certificateFileName)), certificatePassword);

                        options.ConfigureHttpsDefaults(o =>
                        {
                            o.ServerCertificate = cert;
                            o.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
                        });
                    });
                });
    }

Will showcase the creation of a MeMo and how this is attached to a REST request and sent to the correct endpoint in NgDP. It will also enable the reference REST_PUSH recipient-system to receive MeMos from NgDP and create and send back Business Receipts upon receiving these.

### **System sub-projects**

#### **RestPush**

Reference implementation of REST_PUSH protocol sender- and recipient-system. Showcases how REST requests to NgDP can be implemented, utilizing the RestClient provided by SSLClient - And how the recipient-system can provide an endpoint for receiving MeMos from NgDP - as well as the processing of these received MeMos and the creation and sending of Business Receipts back to NgDP.

#### **RestPublishSubscribe**

Reference implementation of REST_PULISH_SUBSCRIBE protocol recipient-system. Showcases how REST calls can be made to fetch information about MeMos currently available for fetching, as well as fetching these one by one from NgDP. Creates and sends back Business Receipts to NgDP.

#### **Sftp**

Reference implementation of SFTP protocol sender-system. Showcases how created MeMos can be bundled to a zip file and uploaded to NgDP's SFTP server - as well as downloading available receipts from the SFTP server.

**Note**: It is important to set the below environment variables at **reference-systems-for-dotnet/Sftp/Properties/launchSettings.json** in order to gain access to NgDP SFTP server. Each SFTP system will have different configuration.
    + USER_NAME
    + PRIVATE_KEY_PATH (absolute path to your private key)
    + SYSTEM_ID (the Id of your SFTP sender system)
    
    
#### **SSLClient**

Implements mutual SSL authentication in the communication between the reference systems and NgDP.

#### **UtilityLibrary**

Provides model and service classes for MeMos and Business Receipts, handling the creation, parsing and logging of MeMos, as well as the creation, sending and logging of positive or negative Business Receipts. - MeMoBuilder: creating MeMos. - MeMoPersistenceService: saving MeMo as XML and creating it as a file. - MeMoLoggingService: logging elements of MeMo messageHeader to console. - ParseMemoService: parsing MessageHeader element of MeMo. - BusinessReceiptFactory: creating positive or negative Business Receipts. - ReceivedBusinessReceiptLoggingService: logging received Business Receipts to console. - SentBusinessReceiptLoggingService: logging responses from sent Business Receipts to console.
