using System;
using Digst.DigitalPost.Systems.Sftp.Sender.Handlers;
using Digst.DigitalPost.Systems.Sftp.Sender.Services.Receipts;
using Digst.DigitalPost.Systems.Sftp.Sender.Services.Sender;
using Digst.DigitalPost.UtilityLibrary.Memos.Configuration;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.MemoBuilder;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Persistence;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Digst.DigitalPost.Systems.Sftp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton(BindMemoConfiguration());

            services.AddSingleton<ISftpSenderService, SftpSenderService>();
            services.AddSingleton<IMemoBuilder, MemoBuilder>();
            services.AddSingleton<IMeMoPersister, MeMoPersister>();

            services.AddScoped<IReceiptFetcherService, ReceiptFetcherService>();
            services.AddScoped<IReceiptMessageHandler, ReceiptMessageHandler>();

            IGlobalConfiguration<MemoryStorage> inMemory = GlobalConfiguration.Configuration.UseMemoryStorage();
            services.AddHangfire(c => c.UseMemoryStorage());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseHangfireServer();

            IServiceProvider serviceProvider = app.ApplicationServices.CreateScope().ServiceProvider;

            ISftpSenderService sftpSenderService = serviceProvider.GetRequiredService<ISftpSenderService>();
            sftpSenderService.CreateAndUploadMemoZip();

            RecurringJob.AddOrUpdate(() =>
                    serviceProvider.GetRequiredService<IReceiptFetcherService>().DownloadReceipts(),
                Environment.GetEnvironmentVariable("SCHEDULER_FIXED_DELAY"));
        }

        private MemoConfiguration BindMemoConfiguration()
        {
            MemoConfiguration config = new MemoConfiguration();
            Configuration.Bind("Sftp:Memo", config);

            return config;
        }
    }
}