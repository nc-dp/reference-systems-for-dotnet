using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Digst.DigitalPost.Systems.Sftp.Properties;
using Digst.DigitalPost.Systems.Sftp.Sender.Factories;
using Digst.DigitalPost.Systems.Sftp.Sender.Handlers;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Services.Receipts
{
    public class ReceiptFetcherService : IReceiptFetcherService
    {
        private readonly ILogger logger;

        private readonly IReceiptMessageHandler receiptMessageHandler;

        public ReceiptFetcherService(ILogger<ReceiptFetcherService> logger,
            IReceiptMessageHandler receiptMessageHandler)
        {
            this.logger = logger;
            this.receiptMessageHandler = receiptMessageHandler;
        }

        public void DownloadReceipts()
        {
            IEnumerable<SftpFile> receiptLists = FetchReceipts();

            DirectoryInfo downloadDirectory = Directory.CreateDirectory(SftpConfig.DownloadPath);

            SftpClient sftpClient = SftpClientFactory.CreateSftpClient();
            try
            {
                sftpClient.Connect();

                foreach (SftpFile receipt in receiptLists)
                {
                    if (!receipt.IsDirectory && !receipt.Name.StartsWith(".") && receipt.Name.EndsWith(".xml"))
                    {
                        string remoteReceiptAbsolutePath = receipt.FullName;
                        string localReceiptFileName = $"{downloadDirectory.FullName}/{receipt.Name}";
                        using FileStream localReceiptFile = File.Create(localReceiptFileName);

                        sftpClient.DownloadFile(remoteReceiptAbsolutePath, localReceiptFile);

                        logger.LogInformation($"Finished downloading file from [{remoteReceiptAbsolutePath}]");

                        localReceiptFile.Close();

                        DeleteRemoteReceiptFile(remoteReceiptAbsolutePath);

                        receiptMessageHandler.HandleReceipt(localReceiptFileName);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.LogError(exception, "Failed in downloading file");
            }
            finally
            {
                sftpClient.Disconnect();
            }
        }

        public IEnumerable<SftpFile> FetchReceipts()
        {
            string remoteDirectory = SftpConfig.ReceiptFolder;

            SftpClient sftpClient = SftpClientFactory.CreateSftpClient();
            try
            {
                sftpClient.Connect();
                return sftpClient.ListDirectory(remoteDirectory);
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"Failed in listing files under [{remoteDirectory}]");
                return Enumerable.Empty<SftpFile>();
            }
            finally
            {
                sftpClient.Disconnect();
            }
        }

        public void DeleteRemoteReceiptFile(string remoteFilePath)
        {
            SftpClient sftpClient = SftpClientFactory.CreateSftpClient();
            try
            {
                sftpClient.Connect();
                sftpClient.DeleteFile(remoteFilePath);
                logger.LogInformation($"File [{remoteFilePath}] deleted.");
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"Failed in deleting file [{remoteFilePath}]");
            }
            finally
            {
                sftpClient.Disconnect();
            }
        }
    }
}