﻿using System.Collections.Generic;
using Renci.SshNet.Sftp;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Services.Receipts
{
    internal interface IReceiptFetcherService
    {
        IEnumerable<SftpFile> FetchReceipts();

        void DownloadReceipts();

        void DeleteRemoteReceiptFile(string remoteFilePath);
    }
}