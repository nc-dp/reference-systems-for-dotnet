using System;
using System.IO;
using Digst.DigitalPost.Systems.Sftp.Properties;
using Digst.DigitalPost.Systems.Sftp.Sender.Factories;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.MemoBuilder;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Persistence;
using Renci.SshNet;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Services.Sender
{
    public class SftpSenderService : ISftpSenderService
    {
        private readonly IMemoBuilder memoBuilder;

        private readonly IMeMoPersister memoPersister;

        public SftpSenderService(IMemoBuilder memoBuilder, IMeMoPersister memoPersister)
        {
            this.memoBuilder = memoBuilder;
            this.memoPersister = memoPersister;
        }

        /// <inheritdoc/>
        public void CreateAndUploadMemoZip()
        {
            using FileStream memoTar = memoBuilder.CreateMemoTar();
            UploadFile(memoTar, memoTar.Name);
        }

        /// <inheritdoc/>
        public void UploadFile(Stream stream, string fullFilename)
        {
            SftpClient sftpClient = SftpClientFactory.CreateSftpClient(false);
            sftpClient.Connect();
            try
            {
                string filename = fullFilename.Substring(fullFilename.LastIndexOf('\\') + 1);
                string tmpPath = $"{SftpConfig.MemoFolder}/{SftpConfig.TmpFolder}/{filename}";
                string path = $"{SftpConfig.MemoFolder}/{filename}";


                sftpClient.UploadFile(stream, tmpPath, true);
                sftpClient.RenameFile(tmpPath, path);
            }
            finally
            {
                sftpClient.Dispose();
            }
        }
    }
}