using System.IO;
using Digst.DigitalPost.Systems.Sftp.Properties;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Services.Sender
{
    public interface ISftpSenderService
    {
        /// <summary>
        ///     Uploads data to the SFTP server using the <see cref="SftpConfig" /> settings.
        /// </summary>
        /// <param name="stream">Input stream containing the data to upload.</param>
        /// <param name="fullFilename">Filename of the remote destination file.</param>
        void UploadFile(Stream stream, string fullFilename);

        /// <summary>
        ///     Creates a memo tar file and uploads it to the SFTP server using the <see cref="SftpConfig" /> settings.
        /// </summary>
        void CreateAndUploadMemoZip();
    }
}