﻿using System.IO;
using Microsoft.Extensions.Logging;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Handlers
{
    public class ReceiptMessageHandler : IReceiptMessageHandler
    {
        private readonly ILogger logger;

        public ReceiptMessageHandler(ILogger<ReceiptMessageHandler> logger)
        {
            this.logger = logger;
        }

        public void HandleReceipt(string receiptFilePath)
        {
            logger.LogInformation("Received receipt: {}", File.ReadAllText(receiptFilePath));

            File.Delete(receiptFilePath);
        }
    }
}