﻿namespace Digst.DigitalPost.Systems.Sftp.Sender.Handlers
{
    public interface IReceiptMessageHandler
    {
        void HandleReceipt(string receiptFilePath);
    }
}