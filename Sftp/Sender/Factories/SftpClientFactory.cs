﻿using Digst.DigitalPost.Systems.Sftp.Properties;
using Renci.SshNet;

namespace Digst.DigitalPost.Systems.Sftp.Sender.Factories
{
    /// <summary>
    ///     SFTP Client Factory.
    /// </summary>
    public class SftpClientFactory
    {
        /// <summary>
        ///     Creates a SFTP Client based on the settings from <see cref="SftpConfig" />.
        /// </summary>
        /// <param name="usePrivateKey">
        ///     <c>true</c> means that a private key must be used;
        ///     <c>false</c> means that the user password will be used.
        /// </param>
        /// <returns>A <see cref="SftpClient" /> instance.</returns>
        /// <seealso cref="SftpClient" />
        /// <seealso cref="SftpConfig" />
        public static SftpClient CreateSftpClient(bool usePrivateKey = true)
        {
            PrivateKeyFile privateKey = new PrivateKeyFile(SftpConfig.PrivateKeyPath, SftpConfig.SshKeyPassword);
            return new SftpClient(
                SftpConfig.Host,
                int.Parse(SftpConfig.Port),
                SftpConfig.SshUsername,
                privateKey);
        }
    }
}