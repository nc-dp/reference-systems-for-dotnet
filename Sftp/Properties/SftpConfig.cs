﻿using System;

namespace Digst.DigitalPost.Systems.Sftp.Properties
{
    /// <summary>
    ///     SFTP Configuration based on the settings from environment variables.
    /// </summary>
    public class SftpConfig
    {
        public static string Host => Environment.GetEnvironmentVariable("SFTP_HOST");

        public static string Port => Environment.GetEnvironmentVariable("SFTP_PORT");

        public static string MemoFolder => Environment.GetEnvironmentVariable("MEMO_FOLDER");

        public static string ReceiptFolder => Environment.GetEnvironmentVariable("RECEIPT_FOLDER");
        
        public static string TmpFolder => Environment.GetEnvironmentVariable("TMP_FOLDER");

        public static string SshKeyPassword => Environment.GetEnvironmentVariable("SSH_KEY_PASSWORD");

        public static string PrivateKeyPath => Environment.GetEnvironmentVariable("PRIVATE_KEY_PATH");

        public static string SshUsername => Environment.GetEnvironmentVariable("SSH_USERNAME");

        public static string DownloadPath => Environment.GetEnvironmentVariable("DOWNLOAD_PATH");
    }
}