﻿using System.Net.Http;
using System.Threading.Tasks;
using Digst.DigitalPost.SSLClient.Clients;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;

namespace Digst.DigitalPost.SSLClient.Test
{
    [TestClass]
    public class RestClientTests
    {
        private static WireMockServer _wireMockServer;

        private const int WireMockPort = 8443;

        private static RestClient _restClient;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Mock<ILogger<RestClient>> loggerMock = new Mock<ILogger<RestClient>>();
            HttpClient httpClient = new HttpClient();
            _restClient = new RestClient(loggerMock.Object, httpClient);

            SetUpWireMock();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            _wireMockServer.Dispose();
        }

        private static void SetUpWireMock()
        {
            _wireMockServer = WireMockServer.Start(new WireMockServerSettings
            {
                Urls = new[] {$"http://localhost:{WireMockPort}"}
            });

            _wireMockServer
                .Given(Request.Create().WithPath("/dummy-endpoint").UsingGet())
                .RespondWith(Response.Create().WithStatusCode(200).WithBody("Hello world"));

            _wireMockServer
                .Given(Request.Create().WithPath("/dummy-endpoint").UsingPost())
                .RespondWith(Response.Create().WithStatusCode(201).WithBody("Data Created"));
        }

        [TestMethod]
        public void Get_Should_Return_OK_Response()
        {
            Task<HttpResponseMessage> result = _restClient.Get($"http://localhost:{WireMockPort}/dummy-endpoint", null, null);

            Assert.AreEqual("Hello world", result.Result.Content.ReadAsStringAsync().Result);
        }


        [TestMethod]
        public void Post_Should_Return_OK_Response()
        {
            Task<HttpResponseMessage> result = _restClient.Post($"http://localhost:{WireMockPort}/dummy-endpoint", null, null);

            Assert.AreEqual("Data Created", result.Result.Content.ReadAsStringAsync().Result);
        }
    }
}