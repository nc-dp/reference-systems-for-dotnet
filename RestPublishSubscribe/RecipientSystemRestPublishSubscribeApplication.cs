using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Digst.DigitalPost.Systems.RestPublishSubscribe.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe
{
    public class RecipientSystemRestPublishSubscribeApplication
    {
        private static readonly string ServerCertificateLocation = "SERVER_CERTIFICATE_LOCATION";

        private static readonly string ServerCertificatePassword = "SERVER_CERTIFICATE_PASSWORD";

        public static async Task Main(string[] args)
        {
            IHost host = CreateHostBuilder(args).Build();
            using (IServiceScope serviceScope = host.Services.CreateScope())
            {
                IServiceProvider services = serviceScope.ServiceProvider;
                try
                {
                    IService myService = services.GetRequiredService<IService>();
                    await myService.PullAvailableMemos();
                }
                catch (Exception ex)
                {
                    ILogger<RecipientSystemRestPublishSubscribeApplication> logger =
                        services.GetRequiredService<ILogger<RecipientSystemRestPublishSubscribeApplication>>();
                    logger.LogError(ex, "An error occurred.");
                }
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureKestrel(options =>
                    {
                        string certificateFileName = Environment.GetEnvironmentVariable(ServerCertificateLocation);
                        string certificatePassword = Environment.GetEnvironmentVariable(ServerCertificatePassword);

                        X509Certificate2 cert =
                            new X509Certificate2(File.ReadAllBytes(Path.Combine(certificateFileName)),
                                certificatePassword);

                        options.ConfigureHttpsDefaults(o =>
                        {
                            o.ServerCertificate = cert;
                            o.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
                        });
                    });
                });
        }
    }
}