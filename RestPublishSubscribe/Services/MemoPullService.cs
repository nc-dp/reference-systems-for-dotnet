using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Digst.DigitalPost.SSLClient.Clients;
using Digst.DigitalPost.Systems.RestPublishSubscribe.Models;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Logging;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Parser;
using Digst.DigitalPost.UtilityLibrary.Receipts.Configuration;
using Digst.DigitalPost.UtilityLibrary.Receipts.Models;
using Digst.DigitalPost.UtilityLibrary.Receipts.Sender;
using Digst.DigitalPost.UtilityLibrary.Receipts.Services;
using Dk.Digst.Digital.Post.Memolib.Model;
using Microsoft.Extensions.Logging;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe.Services
{
    public interface IService
    {
        Task PullAvailableMemos();
        Task ProcessMeMo(Guid memoGuid);
        Task SendReceipt(Stream memoStream, Guid memoGuid);
    }

    public class MemoPullService : IService
    {
        private readonly IBusinessReceiptFactory businessReceiptFactory;

        private readonly IBusinessReceiptService businessReceiptService;

        private readonly ILogger logger;

        private readonly IMemoLogging memoLoggingService;

        private readonly IMemoService memoService;

        private readonly RestClient restClient;

        private readonly RecipientSystemConfiguration recipientSystemConfiguration;


        public MemoPullService(ILogger<MemoPullService> logger, RestClient restClient,
            IMemoLogging memoLoggingService, IMemoService memoService,
            IBusinessReceiptFactory businessReceiptFactory, IBusinessReceiptService businessReceiptService,
            RecipientSystemConfiguration recipientSystemConfiguration)
        {
            this.restClient = restClient;
            this.memoLoggingService = memoLoggingService;
            this.memoService = memoService;
            this.businessReceiptFactory = businessReceiptFactory;
            this.businessReceiptService = businessReceiptService;
            this.logger = logger;
            this.recipientSystemConfiguration = recipientSystemConfiguration;
        }

        public async Task PullAvailableMemos()
        {
            // PagingArguments, requesting first page with a max of 10 entities.
            PagingArguments pagingArguments = new PagingArguments(0, 10);
            string pagingParameters =
                $"?page={pagingArguments.Page}&size={pagingArguments.Size}";

            string availableMemoListEndpoint = recipientSystemConfiguration.AvailableMemoEndpoint + pagingParameters;
            HttpResponseMessage response =
                await restClient.Get(availableMemoListEndpoint, recipientSystemConfiguration.ApiKey);

            if (response.IsSuccessStatusCode)
            {
                await using Stream responseStream = await response.Content.ReadAsStreamAsync();
                StreamReader reader = new StreamReader(responseStream);

                JsonSerializer serializer = new JsonSerializer();

                PagingResult<Guid> pagingResult =
                    serializer.Deserialize<PagingResult<Guid>>(new JsonTextReader(reader));
                List<Guid> listOfAvailableMemos = pagingResult?.Content;
                if (listOfAvailableMemos != null)
                {
                    foreach (Guid memoGuid in listOfAvailableMemos)
                    {
                        await ProcessMeMo(memoGuid);
                    }
                }
            }
            else
            {
                logger.LogError("Was not able to fetch available memos from NgDP. HttpStatus: {code}",
                    response.StatusCode);
            }
        }

        public async Task ProcessMeMo(Guid memoGuid)
        {
            HttpResponseMessage response = await restClient.Get(
                recipientSystemConfiguration.AvailableMemoEndpoint + memoGuid, recipientSystemConfiguration.ApiKey,
                "application/xml");

            if (response.IsSuccessStatusCode && response.Content != null)
            {
                logger.LogDebug("MeMo with UUID: {memoGuid} successfully fetched from NgDP.", memoGuid);
                await using Stream responseStream = await response.Content.ReadAsStreamAsync();
                await SendReceipt(responseStream, memoGuid);
            }
            else
            {
                logger.LogError("Was not able to fetch MeMo with UUID: {memoGuid}. HttpStatusCode: {statusCode}:",
                    memoGuid, response.StatusCode);
            }
        }

        public async Task SendReceipt(Stream memoStream, Guid memoGuid)
        {
            MessageHeader memoMessageHeader = memoService.ParseMemoHeader(memoStream);

            Receipt receipt;
            if (memoMessageHeader != null)
            {
                receipt = businessReceiptFactory.CreatePositiveBusinessReceipt(memoGuid.ToString());
                memoLoggingService.LogMemoSuccessfullyParsed(receipt, memoMessageHeader);
            }
            else
            {
                receipt = businessReceiptFactory.CreateNegativeBusinessReceipt();
                memoLoggingService.LogMemoUnsuccessfullyParsed(receipt, memoGuid.ToString());
            }

            await businessReceiptService.SendRestBusinessReceiptToNgDP(receipt, memoGuid.ToString());
        }
    }
}