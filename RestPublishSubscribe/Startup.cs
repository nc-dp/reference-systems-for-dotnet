using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Net.Http;
using Digst.DigitalPost.Systems.RestPublishSubscribe.Services;
using Digst.DigitalPost.SSLClient.Clients;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Parser;
using Digst.DigitalPost.UtilityLibrary.Receipts.Services;
using Digst.DigitalPost.UtilityLibrary.Receipts.Sender;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Logging;
using Digst.DigitalPost.UtilityLibrary.Receipts.Configuration;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe
{
    public class Startup
    {
        private readonly string ClientCertificateLocation = "CLIENT_CERTIFICATE_LOCATION";
        private readonly string ClientCertificatePassword = "CLIENT_CERTIFICATE_PASSWORD";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAuthentication(
                CertificateAuthenticationDefaults.AuthenticationScheme)
                .AddCertificate(options =>
                {
                    options.AllowedCertificateTypes = CertificateTypes.All;
                    options.Events = new CertificateAuthenticationEvents
                    {
                        OnCertificateValidated = context =>
                        {
                            Claim[] claims = new[] {
                                    new Claim(ClaimTypes.NameIdentifier, context.ClientCertificate.Subject, ClaimValueTypes.String, context.Options.ClaimsIssuer),
                                    new Claim(ClaimTypes.Name, context.ClientCertificate.Subject, ClaimValueTypes.String, context.Options.ClaimsIssuer)
                            };

                            context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                            context.Success();

                            return Task.CompletedTask;
                        },
                        
                       OnAuthenticationFailed = context => 
                       {
                           ILogger<Startup> logger = context.HttpContext.RequestServices.GetService<ILogger<Startup>>();
                           logger.LogError(context.Exception, "Failed authentication");
                           return Task.CompletedTask;
                       }

                    };
                });


            services.AddSingleton(BindSenderSystemConfiguration());

            services.AddScoped<IMemoService, ParseMemoService>();
            services.AddScoped<IBusinessReceiptFactory, BusinessReceiptFactory>();
            services.AddScoped<IBusinessReceiptService, BusinessReceiptService>();
            services.AddScoped<IMemoLogging, MemoLoggingService>();
            services.AddScoped<IService, MemoPullService>();
            services.AddScoped<ILogger, Logger<MemoPullService>>();
            services.AddHttpClient<RestClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    HttpClientHandler handler = new HttpClientHandler();
                    handler.ClientCertificates.Add(LoadClientCertificate());
                    return handler;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Console.WriteLine("HostingEnvironmentName: '{0}'", env.EnvironmentName);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            } else 
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseStaticFiles();
        }

        private RecipientSystemConfiguration BindSenderSystemConfiguration()
        {
            RecipientSystemConfiguration config = new RecipientSystemConfiguration();
            Configuration.Bind("RecipientSystem", config);

            return config;
        }

        private X509Certificate2 LoadClientCertificate()
        {
            string certificateFileName = Environment.GetEnvironmentVariable(ClientCertificateLocation);
            string certificatePassword = Environment.GetEnvironmentVariable(ClientCertificatePassword);

            return new X509Certificate2(Path.Combine(certificateFileName), certificatePassword);
        }
    }
}
