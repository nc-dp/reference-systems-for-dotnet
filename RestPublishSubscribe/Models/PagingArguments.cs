using System.Collections.Generic;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe.Models
{
    public class PagingArguments
    {
        public enum Direction
        {
            ASC,

            DESC
        }

        public int Page { get; set; }

        public int Size { get; set; }

        public List<Order> Ordering { get; set; }

        public PagingArguments(int page, int size)
        {
            Page = page;
            Size = size;
            Ordering = new List<Order>();
        }

        public class Order
        {
            private string Property { get; }

            private Direction Direction { get; }

            public Order(string property)
            {
                Property = property;
                Direction = Direction.ASC;
            }

            public override string ToString()
            {
                return string.Format("%s,%s", Property, Direction);
            }
        }
    }
}