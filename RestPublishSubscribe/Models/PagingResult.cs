using System.Collections.Generic;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe.Models
{
    public class PagingResult<T>
    {
        public List<T> Content { get; set; }

        public int Number { get; set; }

        public int Size { get; set; }

        public PagingArguments Previous { get; set; }

        public PagingArguments Next { get; set; }

        public long TotalElements { get; set; }

        public int TotalPages { get; set; }

        public PagingResult(
            List<T> content, int number, int size, PagingArguments previous, PagingArguments next)
        {
            Content = content;
            Number = number;
            Size = size;
            Previous = previous;
            Next = next;
        }
    }
}