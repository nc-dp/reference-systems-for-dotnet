﻿using System;
using Digst.DigitalPost.Systems.RestPublishSubscribe.Services;
using Digst.DigitalPost.UtilityLibrary.Memos.Services.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Digst.DigitalPost.Systems.RestPublishSubscribe.APIs
{
    [Route("notification")]
    [ApiController]
    public class NotificationEndpointController : ControllerBase
    {
        private readonly IMemoLogging memoLoggingService;

        private readonly IService memoPullService;

        public NotificationEndpointController(IMemoLogging memoLoggingSerice, IService memoPullService)
        {
            memoLoggingService = memoLoggingSerice;
            this.memoPullService = memoPullService;
        }

        [HttpPost]
        public IActionResult NotificationEndpoint([FromForm] string url)
        {
            if (url != null)
            {
                string[] array = url.Split('/');
                string memoGuid = array[array.Length - 1];
                memoPullService.ProcessMeMo(new Guid(memoGuid));
                return Ok($"Be notified with url: {memoGuid}. Start fetching the given memo and process it after that");
            }

            return StatusCode(400, "The notification request does not contain a proper memo url");
        }
    }
}