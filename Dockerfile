#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["reference-systems-for-dotnet/reference-systems-for-dotnet.csproj", "reference-systems-for-dotnet/"]
RUN dotnet restore "reference-systems-for-dotnet/reference-systems-for-dotnet.csproj"
COPY . .
WORKDIR "/src/reference-systems-for-dotnet"
RUN dotnet build "reference-systems-for-dotnet.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "reference-systems-for-dotnet.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "reference-systems-for-dotnet.dll"]